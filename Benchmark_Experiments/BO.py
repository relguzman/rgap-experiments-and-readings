import numpy as np
from scipy import optimize
from GP import GP

class BO:
    """ Same but simplistic GPyOpt's BayesianOptimization implementation
    """
    
    def __init__(self, f, domain, model_type, acquisition_type):
        self.f = f
        
        self.X_sample = []
        self.Y_sample = []
        self.domain = np.array([d['domain'] for d in domain])
        
        ## Surrogate Model
        if model_type is 'GP':
            # RBF Kernel
            self.model_type = GP(GP.RBF, params=[1, 0.2]) # TODO
            
        ## Acquisition Function
        if acquisition_type is 'UCB':
            def ucb(X, X_sample, Y_sample, surrogate, balance=0.8):
                mu, variance = surrogate.predict(X, return_std=True)
                return mu + balance * variance    
            def obj(X):
                # Minimization objective is the negative acquisition function
                x_new = np.array([X])
                return ucb(x_new, self.X_sample, self.Y_sample, surrogate=self.model_type)
            self.acquisition_type = obj
        pass
    
    def run_optimization(self, max_iter):
        
        initial_design_numdata = 1
        for i in range(initial_design_numdata):
            x0 = [np.random.uniform(d[0], d[1]) for d in self.domain]
            X_next = np.array([x0])
            Y_next = self.f(X_next)
            # Add sample to initial samples
            self.X_sample = np.append(self.X_sample, X_next)
            self.Y_sample = np.append(self.Y_sample, Y_next)
        print("Initialized")
        self.X_sample = np.array([self.X_sample])
        self.Y_sample = np.array([self.Y_sample])
    
        for i in range(max_iter):
            self.model_type.fit(self.X_sample, self.Y_sample)

            # Minimize and obtain next sampling point
            ### Minimize brute force
#             output = optimize.brute(self.acquisition_type, self.domain)
#             X_next = np.array([output])

            ### Minimize default
            x0 = np.array([[np.random.uniform(d[0], d[1]) for d in self.domain]])
            output = optimize.minimize(self.acquisition_type, x0=x0, bounds=self.domain).x
            X_next = np.array([output])
            
            # print(output)
            
            # Obtain next sample from the objective function
            Y_next = self.f(X_next)
            # print("x = {}, y = {}".format(X_next, Y_next))

            # Add sample to previous samples
            self.X_sample = np.concatenate((self.X_sample, X_next), axis=0)
            self.Y_sample = np.append(self.Y_sample, Y_next)
    def get_evaluations(self):
        return self.X_sample, self.Y_sample
