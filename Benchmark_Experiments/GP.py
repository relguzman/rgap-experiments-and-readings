import numpy as np

class GP:
    """
    https://github.com/matthias-wright/sarcos/blob/master/models/gaussian_processes.py
    """
    
    def __init__(self, kernel, params):
        self.X_train = None
        self.Y_train = None
        self.kernel = kernel
        self.params = params
        pass
    
    def fit(self, X_train, Y_train):
        self.X_train = X_train
        self.Y_train = Y_train
        return

    def draw_from_prior(self, X_test, num_samples):
        """
        Returns samples drawn from the prior distribution at the test inputs.
        :param num_samples: number of samples to be drawn
        :return: samples
        """
        K__star_star = GaussianProcess.RBF(X_test, X_test, self.params)
        return np.dot(K__star_star, np.random.normal(size=(len(X_test), num_samples)))

    def draw_from_posterior(self, X_test, num_samples):
        """
        Returns samples drawn from the posterior at the test inputs.
        :param num_samples: number of samples to be drawn
        :return: samples
        """
        K = GaussianProcess.RBF(self.X_train, self.X_train, self.params)
        ## Moore-Penrose) pseudo-inverse of a matrix.
        K_inverse = np.linalg.pinv(K)
#         K_inverse = np.linalg.inv(K)
        K__star_star = GaussianProcess.RBF(X_test, X_test, self.params)
        K_star_ = GaussianProcess.RBF(X_test, self.X_train, self.params)
        K__star = GaussianProcess.RBF(self.X_train, X_test, self.params)
        K_conditioned = K__star_star - np.dot(np.dot(K_star_, K_inverse), K__star)
        mu = np.dot(np.dot(K_star_, K_inverse), self.Y_train)
        return mu + np.dot(K_conditioned, np.random.normal(size=(len(X_test), num_samples)))

    def predict(self, X_test, return_std=True):
        """
        Predicts the output values for the provided inputs.
        :return: predictions, variance for the predictions
        """
        K = GP.RBF(self.X_train, self.X_train, self.params)
        ## Moore-Penrose) pseudo-inverse of a matrix.
        K_inverse = np.linalg.pinv(K)
#         K_inverse = np.linalg.inv(K)
        K__star_star = GP.RBF(X_test, X_test, self.params)
        K_star_ = GP.RBF(X_test, self.X_train, self.params)
        K__star = GP.RBF(self.X_train, X_test, self.params)
        K_conditioned = K__star_star - np.dot(np.dot(K_star_, K_inverse), K__star)
        sigma_squared = np.diag(K_conditioned)
        mu = np.dot(np.dot(K_star_, K_inverse), self.Y_train)
        
        if return_std:
            return mu, sigma_squared
        return mu

    def predict_cholesky(self, X_test, return_std=True):
        """
        Predicts the output values for the provided inputs. Uses the Cholesky decomposition (more on that in the report)
        in order to reduce computation and improve numerical stability.
        :return: predictions, variance for the predictions
        """
        K = GP.RBF(self.X_train, self.X_train, self.params)
        L = np.linalg.cholesky(K)
        K_star_ = GP.RBF(X_test, self.X_train, self.params)
        v = np.linalg.solve(L, self.Y_train)
        w = np.linalg.solve(L.T, v)
        mu = np.dot(K_star_, w)
        q = np.linalg.solve(L, K_star_.T)
        z = np.linalg.solve(L.T, q)
        K__star_star = GP.RBF(X_test, X_test, self.params)
        K_conditioned = K__star_star - np.dot(K_star_, z)
        sigma_squared = np.diag(K_conditioned)
        
        if return_std:
            return mu, sigma_squared
        return mu

    def test(self, X_test, Y_test, use_cholesky=False):
        """
        Tests the algorithm on the provided test set.
        :param use_cholesky: True: use Cholesky decomposition for computation, False: do not
        :return: RMSE for the test set, predictions on the test set, variance for the predictions
        """
        if use_cholesky:
            mu, sigma = self.predict_cholesky(X_test, self.params)
        else:
            mu, sigma = self.predict(X_test, self.params)
        rmse = np.sqrt(np.mean(np.square(mu - Y_test)))
        return rmse, mu, sigma

    @staticmethod
    def RBF(x1, x2, params): # TODO
        """
        Returns the covariance matrix according to the squared exponential kernel between x1 and x2.
        :return: Covariance matrix
        """
        dist = np.sum(x1**2, axis=1, keepdims=True) + np.sum(x2**2, axis=1) - 2 * np.dot(x1, x2.T)
        return params[0]**2 * np.exp(-(1 / (2 * params[1]**2)) * dist)
