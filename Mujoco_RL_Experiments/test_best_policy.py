from mjrl.utils.gym_env import GymEnv
from mjrl.policies.gaussian_mlp import MLP
from mjrl.baselines.quadratic_baseline import QuadraticBaseline
from mjrl.baselines.mlp_baseline import MLPBaseline
from mjrl.algos.npg_cg import NPG
from mjrl.algos.behavior_cloning import BC
from mjrl.utils.train_agent import train_agent
from mjrl.samplers.trajectory_sampler import sample_paths
import mjrl.envs
import time as timer
import pickle

# ------------------------------
# Train expert policy first
e = GymEnv('Swimmer-v2')

# ------------------------------
# Get demonstrations
print("========================================")
print("Collecting expert demonstrations")
print("========================================")
expert_pol = pickle.load(open('swimmer_exp1/iterations/best_policy.pickle', 'rb'))

# ------------------------------
# Evaluate Policies
expert_score = e.evaluate_policy(expert_pol, num_episodes=25, visual=True)
print("Expert policy performance (eval mode) = %f" % expert_score[0][0])

